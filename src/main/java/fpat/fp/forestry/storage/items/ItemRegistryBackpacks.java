package fpat.fp.forestry.storage.items;

import javax.annotation.Nullable;

import net.minecraft.item.Item;
import forestry.api.storage.BackpackManager;
import forestry.api.storage.EnumBackpackType;
import forestry.api.storage.IBackpackInterface;
import forestry.core.items.ItemRegistry;
import fpat.fp.forestry.core.FPATForestry;
import fpat.fp.forestry.storage.FPATForestryBackpackManager;

public class ItemRegistryBackpacks extends ItemRegistry {

	public final Item combBackpack;
	public final Item combBackpackT2;
	public final Item combBackpackT3;

	public ItemRegistryBackpacks() {
		// BACKPACKS
		IBackpackInterface backpackInterface = BackpackManager.backpackInterface;

		combBackpack = registerItem(backpackInterface.createBackpack(FPATForestryBackpackManager.COMB_UID, EnumBackpackType.NORMAL), "comb_bag");
		combBackpack.setCreativeTab(FPATForestry.FPTab);
		
		combBackpackT2 = registerItem(backpackInterface.createBackpack(FPATForestryBackpackManager.COMB_UID, EnumBackpackType.WOVEN), "comb_bag_t2");
		combBackpackT2.setCreativeTab(FPATForestry.FPTab);

		combBackpackT3 = registerItem(backpackInterface.createBackpack(FPATForestryBackpackManager.COMB_UID, EnumBackpackType.NATURALIST), "comb_bag_t3");
		combBackpackT3.setCreativeTab(FPATForestry.FPTab);
		}
}