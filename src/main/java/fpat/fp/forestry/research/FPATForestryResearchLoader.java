package fpat.fp.forestry.research;

import java.io.InputStreamReader;

import futurepack.common.FPLog;
import futurepack.common.research.ResearchLoader;

public class FPATForestryResearchLoader
{
	
	public static void init()
	{
		try
		{
			ResearchLoader.instance.addResearchesFromReader(new InputStreamReader(FPATForestryResearchLoader.class.getResourceAsStream("research.json")));
		}
		catch(Exception ex)
		{
			FPLog.logger.error("Failed to load research.json for FpatForestry!");
			ex.printStackTrace();
		}
	}
}
