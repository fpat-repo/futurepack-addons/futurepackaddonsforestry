package fpat.fp.forestry.research;

import fpat.fp.forestry.core.FPATForestry;
import fpat.fp.forestry.research.icons.FPATForestryResearchIcons;
import futurepack.api.event.ResearchPageRegisterEvent;
import futurepack.common.research.ResearchLoader;
import futurepack.common.research.ResearchPage;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = FPATForestry.modID)
public class FPATForestryResearchPage
{

	public static ResearchPage fpatforestry;
	
	/**
	 * Used in {@link ResearchLoader} to make sure, <i>/fp research reload</i>, works.
	 */
	
	@SubscribeEvent
    public static void init(ResearchPageRegisterEvent event)
	{
		fpatforestry = new ResearchPage("fpatforestry").setIcon(new ItemStack(FPATForestryResearchIcons.research_tab_pages, 1, 0));
	};
	
}
