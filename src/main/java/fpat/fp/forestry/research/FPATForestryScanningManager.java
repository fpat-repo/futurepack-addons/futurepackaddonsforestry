package fpat.fp.forestry.research;

import java.util.ArrayList;
import java.util.List;

import forestry.apiculture.MaterialBeehive;
import forestry.apiculture.blocks.BlockBeeHives;
import forestry.apiculture.worldgen.Hive;
import forestry.apiculture.worldgen.HiveRegistry;
import fpat.fp.forestry.handler.FPATForestryBlocksHives;
import futurepack.api.EnumScanPosition;
import futurepack.api.interfaces.IScanPart;
import futurepack.common.FPMain;
import futurepack.common.dim.biome.BiomeTyrosRockDesert;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.ResearchManager;
import futurepack.common.research.ScanningManager;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

public class FPATForestryScanningManager extends ScanningManager
{
	public static List<ScanEntry> list = new ArrayList<ScanEntry>();
	
	static
	{
		
		//Flowers
		new ScanEntry(Material.CACTUS,	"fpat.forestry.flowers");
		new ScanEntry(Material.VINE, 	"fpat.forestry.flowers");
		new ScanEntry(Material.PLANTS,  "fpat.forestry.flowers");

		//Plants
		new ScanEntry(Material.WOOD, 	"fpat.forestry.plants");
		
		//Foliage
		new ScanEntry(Material.LEAVES , "fpat.forestry.foliage");

		//Hives
		new ScanEntry(FPATForestryBlocksHives.cybernetics_hive,     "fpat.forestry.hives");
		new ScanEntry(FPATForestryBlocksHives.menelaus_hive,        "fpat.forestry.hives");
		new ScanEntry(FPATForestryBlocksHives.tyros_hive,           "fpat.forestry.hives");
	}
	
	private static class ScanEntry
	{
		final String reserach;
		
		Class<? extends Entity> entity;
		Block bl;
		Material m;
		String ore;
		
		public ScanEntry(Class<? extends Entity> entity, String r) 
		{
			this(r);
			this.entity = entity;
		}
		
		public ScanEntry(Block b, String r) 
		{
			this(r);
			bl=b;
		}
		
		public ScanEntry(Material m, String r) 
		{
			this(r);
			this.m = m;
		}
		
		public ScanEntry(String o, String r) 
		{
			this(r);
			this.ore = o;
		}
		
		private ScanEntry(String r)
		{
			reserach=r;
			list.add(this);
		}

		public boolean onEntity(World w, EntityLivingBase entity, EntityPlayer user)
		{
			checkUnderground(user);
			
			if(this.entity!=null)
			{
				if(this.entity.isAssignableFrom(entity.getClass()))
				{
					add(user);	
					return true;
				}
			}
			return false;
		}
		
		public boolean onBlock(World w, BlockPos pos, EntityPlayer user, RayTraceResult res)
		{
			checkUnderground(user);
			
			if(bl!=null)
			{
				IBlockState state = w.getBlockState(pos);
				Block bl = state.getBlock();
				if(this.bl == bl)
				{
					add(user);	
					return true;
				}
			}
			if(m!=null)
			{
				IBlockState state = w.getBlockState(pos);
				Block bl = state.getBlock();
				if(state.getMaterial() == m)
				{
					add(user);	
					return true;
				}
			}
			if(ore!=null)
			{
				IBlockState state = w.getBlockState(pos);
				Block bl = state.getBlock();
				ItemStack it = bl.getPickBlock(state, res, w, pos, user);				
				if(it==null || it.isEmpty())
				{
					it = new ItemStack(bl, 1, bl.damageDropped(state));
				}
					
				if(!it.isEmpty())
				{
					for(int i : OreDictionary.getOreIDs(it))
					{
						if(ore.equals(OreDictionary.getOreName(i)))
						{
							add(user);	
							return true;
						}
					}
				}
			}
			
			return false;
		}
		
		private static void checkUnderground(EntityPlayer pl) 
		{
			if(pl.posY < 30)
			{
				if(pl.dimension == FPMain.dimMenelausID) 
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.menelaus.caves"));
				}
				else if(pl.dimension == FPMain.dimTyrosID)
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.caves"));
				}
			}
			else if(pl.posY > 60)
			{
				if(pl.dimension == FPMain.dimTyrosID)
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.surface"));
				}
			}
			
			if(pl.world.getBiome(pl.getPosition()) instanceof BiomeTyrosRockDesert)
			{
				CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.deadlands"));
			}
			
		}
		
		private void add(EntityPlayer pl)
		{
			CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch(reserach));
		}
	}
}

