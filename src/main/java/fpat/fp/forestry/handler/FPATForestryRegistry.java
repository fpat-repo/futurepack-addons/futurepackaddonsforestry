package fpat.fp.forestry.handler;

import fpat.fp.forestry.core.FPATForestry;
import fpat.fp.forestry.research.icons.FPATForestryResearchIcons;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = FPATForestry.modID)
public class FPATForestryRegistry {
	
	@SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		if(Loader.isModLoaded("forestry"))
		{
		FPATForestryBlocksHives.register(event);
		}
	}
	
	@SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
	{
		if(Loader.isModLoaded("forestry"))
		{
		event.getRegistry().registerAll(FPATForestryBlocksHives.itemBlocks.toArray(new Item[FPATForestryBlocksHives.itemBlocks.size()]));
		FPATForestryBlocksHives.itemBlocks.clear();
		FPATForestryBlocksHives.itemBlocks = null;
		
		FPATForestryItemsCombs.register(event);
//		FPATForestryItemsFrame.register(event);
		FPATForestryTools.register(event);
		FPATForestryResearchIcons.register(event);
		}
	}
	
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
		if(Loader.isModLoaded("forestry"))
		{
		try
		{
			FPATForestryBlocksHives.setupPreRendering();
		}
		catch(NoSuchMethodError e){}
		FPATForestryBlocksHives.setupRendering();
		FPATForestryItemsFrame.setupRendering(); 
		FPATForestryTools.setupRendering();
		FPATForestryResearchIcons.setupRendering();
		}
	}
}
