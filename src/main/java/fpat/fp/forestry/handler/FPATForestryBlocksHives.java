package fpat.fp.forestry.handler;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import fpat.fp.forestry.apiculture.hive.BlockHiveCybernetics;
import fpat.fp.forestry.apiculture.hive.BlockHiveMenelaus;
import fpat.fp.forestry.apiculture.hive.BlockHiveTyros;
import fpat.fp.forestry.core.FPATForestry;
import futurepack.common.FPLog;
import futurepack.common.block.FPBlocks;
import futurepack.common.item.ItemMetaMultiTex;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAir;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;


public class FPATForestryBlocksHives {

	public static List<Item> itemBlocks = new ArrayList<Item>();
	
	public static final BlockHiveMenelaus menelaus_hive = new BlockHiveMenelaus();
	public static final BlockHiveTyros tyros_hive = new BlockHiveTyros();
	public static final BlockHiveCybernetics cybernetics_hive = new BlockHiveCybernetics();
	
	public static void register(RegistryEvent.Register<Block> event)
	{

		if(Loader.isModLoaded("forestry"))
		{
			IForgeRegistry<Block> r = event.getRegistry();

			registerBlockWithItem(menelaus_hive, "menelaus_hive", r);
			FPBlocks.registerMetaHarvestLevel(menelaus_hive, "scoop", 1);
			registerBlockWithItem(tyros_hive, "tyros_hive", r);
			FPBlocks.registerMetaHarvestLevel(tyros_hive, "scoop", 2);
			registerBlockWithItem(cybernetics_hive, "cybernetics_hive", r);
			FPBlocks.registerMetaHarvestLevel(cybernetics_hive, "scoop", 0);
		}
	}

	private static void registerBlockWithItem(Block bl,	String string, IForgeRegistry<Block> reg)
	{
		ResourceLocation res = new ResourceLocation(FPATForestry.modID, string);
		bl.setRegistryName(res);
		reg.register(bl);
		ItemMetaMultiTex tex = new ItemMetaMultiTex(bl);
		tex.setRegistryName(res);
		itemBlocks.add(tex);
	}
	
	@SideOnly(Side.CLIENT)
	public static void setupPreRendering()
	{
		
	}
	
	@SideOnly(Side.CLIENT)
	public static void setupRendering()
	{		
		try
		{			
			Field[] fields = FPATForestryBlocksHives.class.getFields();
			for(Field f : fields)
			{
				if(Modifier.isStatic(f.getModifiers()))
				{
					Object o = f.get(null);
					if(o instanceof Block)
					{
						Item item = Item.getItemFromBlock((Block) o);
						
						if(item==null || item.getClass() == ItemAir.class)
						{
							FPLog.logger.error("Block %s has no Item!", o);						
							continue;
						}	
						
						if(o instanceof IItemMetaSubtypes)
						{
							IItemMetaSubtypes meta = (IItemMetaSubtypes) o;
							for(int i=0;i<meta.getMaxMetas();i++)
							{
								registerItem(meta.getMetaName(i), item, i);
							}
						}
						else
						{
							registerItem(item.getUnlocalizedName().substring(5), item, 0);
						}
					}
 				}
			}
		}
			
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SideOnly(Side.CLIENT)
	private static void registerItem(String s, Item i, int meta)
	{
		RenderItem render = Minecraft.getMinecraft().getRenderItem();
		ResourceLocation res = new ResourceLocation(FPATForestry.modID, "blocks/" + toLoverCase(s));
		ModelLoader.setCustomModelResourceLocation(i, meta, new ModelResourceLocation(res, "inventory"));
		FPLog.logger.debug(String.format("Add Item %s(%s) with %s",i,meta,s));
	}

	
	private static String toLoverCase(String s)
	{
		for(int i='A';i<='Z';i++)
		{
			int index = s.indexOf(i);
			if(index > 0)
			{
				char c = s.charAt(index -1);
				String s2 = c >= 'a' && c <= 'z' ? "_" : "";
				s = s.replace( "" + (char)i, (s2 +(char)i).toLowerCase() );
			}
			else
			{
				s = s.replace( "" + (char)i, ("" +(char)i).toLowerCase() );
			}
				
		}
		return s;
	}
}
