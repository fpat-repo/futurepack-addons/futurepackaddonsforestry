package fpat.fp.forestry.handler;

import forestry.apiculture.ModuleApiculture;
import forestry.apiculture.genetics.HiveDrop;
import forestry.apiculture.items.EnumHoneyComb;
import fpat.fp.forestry.apiculture.hive.HiveDescription;
import fpat.fp.forestry.apiculture.hive.HiveType;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Loader;

public class FPATForestryWorld {

	public static void RegisterHives()
	{
		if(Loader.isModLoaded("forestry"))
		{
		ModuleApiculture.getHiveRegistry().registerHive(HiveType.Menelaus.getHiveUid(), HiveDescription.MENELAUS);
		ModuleApiculture.getHiveRegistry().registerHive(HiveType.Tyros.getHiveUid(), HiveDescription.TYROS);
			ItemStack honeyComb = ModuleApiculture.getItems().beeComb.get(EnumHoneyComb.HONEY, 1);
//			ItemStack neonComb = new ItemStack(FPFAItemsCombs.Neon);
			ModuleApiculture.getHiveRegistry().addDrops(HiveType.Menelaus.getHiveUid(), new HiveDrop(0.8, BeeDefinition.Menelaus).setIgnobleShare(0.7));
		}
	}

}
