package fpat.fp.forestry.handler;

import fpat.fp.forestry.core.FPATForestry;
import fpat.fp.forestry.items.comb.FPATItemsCombs;
import futurepack.common.block.FPBlocks;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.registries.IForgeRegistry;

public class FPATForestryItemsCombs {

	static FPATItemsCombs beeComb = new FPATItemsCombs(); 
    
	public static void register(RegistryEvent.Register<Item> event)
	{	
		if(Loader.isModLoaded("forestry"))
		{
			IForgeRegistry<Item> r = event.getRegistry();

			beeComb = registerItem(new FPATItemsCombs(), "bee_combs", r);
		}
	}

	private static FPATItemsCombs registerItem(Item item, String string, IForgeRegistry<Item> reg) {

		ResourceLocation res = new ResourceLocation(FPATForestry.modID, string);
		item.setRegistryName(res);
		reg.register(item);
		return beeComb;
	}
}