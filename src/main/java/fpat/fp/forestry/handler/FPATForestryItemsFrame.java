package fpat.fp.forestry.handler;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import forestry.core.items.ItemScoop;
import fpat.fp.forestry.core.FPATForestry;
import fpat.fp.forestry.items.frame.ItemsHiveFrame;
import futurepack.common.FPMain;
import futurepack.common.block.FPBlocks;
import futurepack.common.item.FPItems;
import futurepack.common.item.ItemSpaceship;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

public class FPATForestryItemsFrame 
{
	public static ItemsHiveFrame ironFrame = (ItemsHiveFrame) new ItemsHiveFrame().setProductionMod(1.2F).setLifespanMod(1.2F).setUnlocalizedName("iron_frame");
	public static ItemsHiveFrame neonFrame = (ItemsHiveFrame) new ItemsHiveFrame().setProductionMod(2F).setUnlocalizedName("neon_frame");
	public static ItemsHiveFrame retiumFrame = (ItemsHiveFrame) new ItemsHiveFrame().setUnlocalizedName("retium_frame");
	public static ItemsHiveFrame glowtitFrame = (ItemsHiveFrame) new ItemsHiveFrame().setSelfLighted(true).setSunlightSimulated(true).setSealed(true).setHellish(true).setUnlocalizedName("glowtit_frame");
	public static ItemsHiveFrame bioteriumFrame = (ItemsHiveFrame) new ItemsHiveFrame().setMutationMod(3F).setUnlocalizedName("bioterium_frame");
	public static ItemsHiveFrame impirialFrame = (ItemsHiveFrame) new ItemsHiveFrame().setTerritoryMod(2F).setUnlocalizedName("impirial_Frame");
	
	public static ItemsHiveFrame princessFrame = (ItemsHiveFrame) new ItemsHiveFrame().setProductionMod(0.5F).setUnlocalizedName("princess_frame");
	
	public static void register(RegistryEvent.Register<Item> event)
	{
		if(Loader.isModLoaded("forestry"))
		{
			IForgeRegistry<Item> r = event.getRegistry();

			registerItem(ironFrame, "iron_frame", r);
			registerItem(neonFrame, "neon_frame", r);
			registerItem(retiumFrame, "retium_frame", r);
			registerItem(glowtitFrame, "glowtit_frame", r);
			registerItem(bioteriumFrame, "bioterium_frame", r);
			registerItem(impirialFrame, "impirial_frame", r);
			
			registerItem(princessFrame, "princess_frame", r);
		}
	}

	private static void registerItem(Item item, String string, IForgeRegistry<Item> reg) {

		ResourceLocation res = new ResourceLocation(FPATForestry.modID, string);
		item.setRegistryName(res);
		reg.register(item);
	}
	

	@SideOnly(Side.CLIENT)
	public static void setupRendering()
	{
		
		try
		{
			Field[] fields = FPATForestryItemsFrame.class.getFields();
			for(Field f : fields)
			{
				if(Modifier.isStatic(f.getModifiers()))
				{
					if(Item.class.isAssignableFrom(f.getType()))
					{	
						Item item = (Item) f.get(null);
						
						if(item==null)
							continue;
						
						if(item instanceof IItemMetaSubtypes)
						{
							IItemMetaSubtypes meta = (IItemMetaSubtypes) item;
							for(int i=0;i<meta.getMaxMetas();i++)
							{
								registerItem(meta.getMetaName(i), item, i);
							}
						}
						else
						{
							registerItem(item.getUnlocalizedName().substring(5), item, 0);
						}
					}
 				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SideOnly(Side.CLIENT)
	private static void registerItem(String string, Item item, int meta)
	{
		RenderItem render = Minecraft.getMinecraft().getRenderItem();
		string = FPATForestry.modID +":items/" + toLoverCase(string);
		ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(string, "inventory"));
	}
	
	private static String toLoverCase(String string)
	{
		for(int i='A';i<='Z';i++)
		{
			int index = string.indexOf(i);
			if(index > 0)
			{
				char c = string.charAt(index -1);
				String s2 = c >= 'a' && c <= 'z' ? "_" : "";
				string = string.replace( "" + (char)i, (s2 +(char)i).toLowerCase() );
			}
			else
			{
				string = string.replace( "" + (char)i, ("" +(char)i).toLowerCase() );
			}
				
		}
		return string;
	}
}
