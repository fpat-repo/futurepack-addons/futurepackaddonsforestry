package fpat.fp.forestry.handler;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import forestry.core.items.ItemScoop;
import fpat.fp.forestry.core.FPATForestry;
import fpat.fp.forestry.items.tools.ItemScoopBioterium;
import fpat.fp.forestry.items.tools.ItemScoopComposite;
import fpat.fp.forestry.items.tools.ItemScoopGlowtite;
import fpat.fp.forestry.items.tools.ItemScoopNeon;
import fpat.fp.forestry.items.tools.ItemScoopRetium;
import futurepack.common.FPMain;
import futurepack.common.block.FPBlocks;
import futurepack.common.item.FPItems;
import futurepack.common.item.ItemSpaceship;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

public class FPATForestryTools 
{

	public static Item tools = new ItemSpaceship().setUnlocalizedName("spaceship");//.setTextureName("spaceship");
	

	public static ToolMaterial TMCOMPOSITE = EnumHelper.addToolMaterial("COMPOSITE", 0, 75, 7.0f, 2.0f, 15).setRepairItem(new ItemStack(tools, 1, ItemSpaceship.CompositeMetall));
	public static ToolMaterial TMNEON = EnumHelper.addToolMaterial("NEON", 0, 1600, 250.0f, 5.0f, 15).setRepairItem(new ItemStack(tools, 1, ItemSpaceship.CompositeMetall));
	public static ToolMaterial TMRETIUM = EnumHelper.addToolMaterial("RETIUM", 1, 3200, 250.0f, 5.0f, 15).setRepairItem(new ItemStack(tools, 1, ItemSpaceship.CompositeMetall));
	public static ToolMaterial TMGLOWTITE = EnumHelper.addToolMaterial("GLOWTITE", 1, 3200, 250.0f, 5.0f, 15).setRepairItem(new ItemStack(tools, 1, ItemSpaceship.CompositeMetall));
	public static ToolMaterial TMBIOTERIUM = EnumHelper.addToolMaterial("BIOTERIUM", 2, 4800, 250.0f, 5.0f, 15).setRepairItem(new ItemStack(tools, 1, ItemSpaceship.CompositeMetall));
    
    public static ItemScoopComposite composite_scoop = new ItemScoopComposite(TMCOMPOSITE);
    public static ItemScoopNeon neon_scoop = new ItemScoopNeon(TMNEON);
    public static ItemScoopRetium retium_scoop = new ItemScoopRetium(TMRETIUM);
    public static ItemScoopGlowtite glowtite_scoop = new ItemScoopGlowtite(TMGLOWTITE);
    public static ItemScoopBioterium bioterium_scoop = new ItemScoopBioterium(TMBIOTERIUM);
	
	
	public static void register(RegistryEvent.Register<Item> event)
	{
		if(Loader.isModLoaded("forestry"))
		{
			IForgeRegistry<Item> r = event.getRegistry();

			registerItem(composite_scoop, "composite_scoop", r);
			registerItem(neon_scoop, "neon_scoop", r);
			registerItem(retium_scoop, "retium_scoop", r);
			registerItem(glowtite_scoop, "glowtite_scoop", r);
			registerItem(bioterium_scoop, "bioterium_scoop", r);
		}
	}

	private static void registerItem(Item item, String string, IForgeRegistry<Item> reg) {

		ResourceLocation res = new ResourceLocation(FPATForestry.modID, string);
		item.setRegistryName(res);
		reg.register(item);
	}
	

	@SideOnly(Side.CLIENT)
	public static void setupRendering()
	{
		
		try
		{
			Field[] fields = FPATForestryTools.class.getFields();
			for(Field f : fields)
			{
				if(Modifier.isStatic(f.getModifiers()))
				{
					if(Item.class.isAssignableFrom(f.getType()))
					{	
						Item item = (Item) f.get(null);
						
						if(item==null)
							continue;
						
						if(item instanceof IItemMetaSubtypes)
						{
							IItemMetaSubtypes meta = (IItemMetaSubtypes) item;
							for(int i=0;i<meta.getMaxMetas();i++)
							{
								registerItem(meta.getMetaName(i), item, i);
							}
						}
						else
						{
							registerItem(item.getUnlocalizedName().substring(5), item, 0);
						}
					}
 				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SideOnly(Side.CLIENT)
	private static void registerItem(String string, Item item, int meta)
	{
		RenderItem render = Minecraft.getMinecraft().getRenderItem();
		string = FPATForestry.modID +":items/" + toLoverCase(string);
		ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(string, "inventory"));
	}
	
	private static String toLoverCase(String string)
	{
		for(int i='A';i<='Z';i++)
		{
			int index = string.indexOf(i);
			if(index > 0)
			{
				char c = string.charAt(index -1);
				String s2 = c >= 'a' && c <= 'z' ? "_" : "";
				string = string.replace( "" + (char)i, (s2 +(char)i).toLowerCase() );
			}
			else
			{
				string = string.replace( "" + (char)i, ("" +(char)i).toLowerCase() );
			}
				
		}
		return string;
	}
}
