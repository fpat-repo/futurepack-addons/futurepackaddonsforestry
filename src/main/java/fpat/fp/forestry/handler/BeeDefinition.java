package fpat.fp.forestry.handler;

import java.awt.Color;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

import com.google.common.base.Preconditions;
import forestry.apiculture.ModuleApiculture;
import forestry.core.ModuleCore;
import net.minecraft.item.Item;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.MinecraftForge;
import forestry.Forestry;
import forestry.api.apiculture.*;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.AlleleSpeciesRegisterEvent;
import forestry.api.genetics.IAllele;
import forestry.apiculture.genetics.*;
import forestry.apiculture.genetics.alleles.AlleleBeeSpecies;
import forestry.apiculture.genetics.alleles.AlleleEffects;
import forestry.core.genetics.IBranchDefinition;
import forestry.core.genetics.alleles.AlleleHelper;
import forestry.core.genetics.alleles.EnumAllele;
import fpat.fp.forestry.apiculture.bees.BeeBranches;
import fpat.fp.forestry.core.FPATForestry;
import fpat.fp.forestry.items.comb.FPATEnumHoneyComb;
import futurepack.client.creative.TabFB_Base;

import javax.annotation.Nonnull;

public enum BeeDefinition implements IBeeDefinition
{
	
	Menelaus(BeeBranches.Menelaus, "Menelaus", true, new Color(0xcd7742), new Color(0xbb6332))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Neon), 0.40f)
//			          .setTemperature(EnumTemperature.HOT).setHumidity(EnumHumidity.ARID);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {

		}
	},Modern(BeeBranches.Menelaus, "Modern", true, new Color(0x55463c), new Color(0x6c7d95))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Neon), 0.40f)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(forestry.apiculture.genetics.BeeDefinition.DILIGENT, Menelaus, 15);
		}
	},Neon(BeeBranches.Menelaus, "Menelaus", true, new Color(0x55463c), new Color(0x42c3ff))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack((FPFAItemsCombs.Neon), 1, FPFAItemsCombs.Neon), 0.05f)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Menelaus, Modern, 12);
		}
	},Retium(BeeBranches.Menelaus, "Menelaus", true, new Color(0x55463c), new Color(0xe41515))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack((FPFAItemsCombs.Retium), 1, FPFAItemsCombs.Retium), 0.05f)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Neon, Modern, 10);
		}
	},Glowtit(BeeBranches.Menelaus, "Menelaus", true, new Color(0x55463c), new Color(0xebfe13))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {
			
//			beeSpecies.addProduct(new ItemStack((FPFAItemsCombs.Glowtit), 1, FPFAItemsCombs.Glowtit), 0.05f)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(forestry.apiculture.genetics.BeeDefinition.SINISTER, Neon, 10);
		}
	},Bioterium(BeeBranches.Menelaus, "Menelaus", true, new Color(0x55463c), new Color(0x24ff46))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack((FPFAItemsCombs.Bioterium), 1, FPFAItemsCombs.Bioterium), 0.05f)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Alutin, Retium, 5);
		}
	},Alutin(BeeBranches.Menelaus, "Menelaus", true, new Color(0x55463c), new Color(0xd4e4e7))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack((FPFAItemsCombs.Alutin), 1, FPFAItemsCombs.Alutin), 0.05f)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Menelaus, Modern, 15);
		}
	},Magnet(BeeBranches.Menelaus, "Menelaus", true, new Color(0x55463c), new Color(0x4d5874))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack((FPFAItemsCombs.Magnetic), 1, FPFAItemsCombs.Magnetic), 0.05f)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Alutin, Modern, 10);
		}
	},Metalic(BeeBranches.Menelaus, "Menelaus", true, new Color(0x7d838e), new Color(0x949494))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic, 1, FPFAItemsCombs.Magnetic), 0.05F)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
		}
	},Cybernetics(BeeBranches.Cybernetics, "Cybernetics", true, new Color(0x7d838e), new Color(0xbbeade))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic, 1, FPFAItemsCombs.Magnetic), 0.05F)
//			.setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Metalic, Menelaus, 20);
		}
	},Futuristic(BeeBranches.Cybernetics, "Cybernetics", true, new Color(0x7d838e), new Color(0xc78b2e))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic, 1, FPFAItemsCombs.Magnetic), 0.05F)
//			.setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Metalic, Cybernetics, 20);
		}
	},Robotic(BeeBranches.Cybernetics, "Cybernetics", true, new Color(0x7d838e), new Color(0xcfcfcf))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic), 0.40f)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Metalic, Futuristic, 15);
		}
	},Neural(BeeBranches.Cybernetics, "Cybernetics", true, new Color(0x7d838e), new Color(0x98f893))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic, 1, FPFAItemsCombs.Magnetic), 0.05F)
//			.setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Futuristic, Cybernetics, 15);
		}
	},Construction(BeeBranches.Cybernetics, "Cybernetics", true, new Color(0x7d838e), new Color(0x7757b8))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic, 1, FPFAItemsCombs.Magnetic), 0.05F)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Futuristic, Neural, 8);
		}
	},Inteligent(BeeBranches.Cybernetics, "Cybernetics", true, new Color(0x8bb1da), new Color(0x395ea0))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic, 1, FPFAItemsCombs.Magnetic), 0.05F)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Futuristic, Robotic, 2);
		}
	},Nanobite(BeeBranches.Cybernetics, "Cybernetics", true, new Color(0x8bb1da), new Color(0xffd606))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic, 1, FPFAItemsCombs.Magnetic), 0.05F)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			registerMutation(Construction, Inteligent, 1);
		}
	},Tyros(BeeBranches.Tyros, "Tyros", true, new Color(0x8bb1da), new Color(0xffd606))  {
		
		@Override
		protected void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies) {

//			beeSpecies.addProduct(new ItemStack(FPFAItemsCombs.Magnetic, 1, FPFAItemsCombs.Magnetic), 0.05F)
//			          .setTemperature(EnumTemperature.NORMAL).setHumidity(EnumHumidity.NORMAL);
		}

		@Override
		protected void setAlleles(IAllele[] template) {
			
//			IAllele[] array = super.getChromosomes();
//			BreedingManager.set(template, EnumBeeChromosome.SPEED, BreedingManager.getEnumAllele("Speed", "SLOW"));
//			BreedingManager.set(template, EnumBeeChromosome.LIFESPAN, BreedingManager.getEnumAllele("Lifespan", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.FERTILITY, BreedingManager.getEnumAllele("Fertility", "NORMAL"));
//			BreedingManager.set(template, EnumBeeChromosome.TEMPERATURE_TOLERANCE, BreedingManager.getEnumAllele("Tolerance", "DOWN_2"));
//			BreedingManager.set(template, EnumBeeChromosome.NEVER_SLEEPS, false);
//			BreedingManager.set(array, EnumBeeChromosome.HUMIDITY, null);
//			BreedingManager.set(template, EnumBeeChromosome.HUMIDITY_TOLERANCE, BreedingManager.getEnumAllele("Tolerance" ,"NONE"));
//			BreedingManager.set(template, EnumBeeChromosome.TOLERATES_RAIN, false);
//			BreedingManager.set(template, EnumBeeChromosome.CAVE_DWELLING, false);
//			BreedingManager.set(template, EnumBeeChromosome.FLOWER_PROVIDER, BreedingManager.getEnumAllele("Flowers", "CACTI"));
//			BreedingManager.set(template, EnumBeeChromosome.FLOWERING, BreedingManager.getEnumAllele("Flowering", "SLOWEST"));
//			BreedingManager.set(template, EnumBeeChromosome.TERRITORY, BreedingManager.getEnumAllele("Territory", "LARGEST"));
//			BreedingManager.set(template, EnumBeeChromosome.EFFECT, BreedingManager.getEffect("None"));
//			
//			return template;
//			
		}

		@Override
		protected void registerMutations() {
			// TODO Auto-generated method stub
			
		}
	};


	// forestry bees
	private static final IAlleleBeeSpecies[] hiveBees = {
	};

	private final IBranchDefinition branch;
	private final IAlleleBeeSpecies species;

	private IAllele[] template;
	private IBeeGenome genome;

	BeeDefinition(IBranchDefinition branch, String binomial, boolean dominant, Color primary, Color secondary)
	{
		String species = toString().toLowerCase(Locale.ENGLISH);
		String modId = FPATForestry.modID;
		String uid = modId + ".species." + species;

		String description = modId + ".description." + species;
		String name = "for.bees.species." + species;

		this.branch = branch;
		if (branch != null){
			IAlleleBeeSpeciesBuilder speciesBuilder = BeeManager.beeFactory.createSpecies(modId, uid, dominant, "TheRealM18", name, description, branch.getBranch(), binomial, primary.getRGB(), secondary.getRGB());
			if(isSecret())
			{
				speciesBuilder.setIsSecret();
			}
			setSpeciesProperties(speciesBuilder);
			this.species = speciesBuilder.build();
		}
		else
			this.species = null;

	}

	protected IAlleleBeeSpecies getAllele() {
		return null;
	}

	protected abstract void setSpeciesProperties(IAlleleBeeSpeciesBuilder beeSpecies);

	protected abstract void setAlleles(IAllele[] template);

	protected abstract void registerMutations();

	protected boolean isSecret()
	{
		return false;
	}

	protected boolean isNeedRegister() {
		return branch != null;
	}

	public static void initBees()
	{
		for (BeeDefinition bee : values())
			if (bee.isNeedRegister())
				bee.init();

		for (BeeDefinition bee : values())
			if (bee.isNeedRegister())
				bee.registerMutations();
	}
	private void init()
	{
		template = branch.getTemplate();
		AlleleHelper.getInstance().set(template, EnumBeeChromosome.SPECIES, species);
		setAlleles(template);

		genome = BeeManager.beeRoot.templateAsGenome(template);

		BeeManager.beeRoot.registerTemplate(template);
	}

	public IBeeMutationBuilder registerMutation(IBeeDefinition allele0, IBeeDefinition allele1, int chance) {
		return registerMutation(allele0.getGenome().getPrimary(), allele1.getGenome().getPrimary(), getTemplate(), chance);
	}

	public static IBeeMutationBuilder registerMutation(IBeeDefinition allele0, IBeeDefinition allele1, IBeeDefinition mutation, int chance) {
		return registerMutation(allele0.getGenome().getPrimary(), allele1.getGenome().getPrimary(), mutation.getTemplate(), chance);
	}

	public static IBeeMutationBuilder registerMutation(IAlleleBeeSpecies allele0, IAlleleBeeSpecies allele1, IAllele[] template, int chance) {
		Preconditions.checkNotNull(allele0);
		Preconditions.checkNotNull(allele1);
		Preconditions.checkNotNull(template);
		return BeeManager.beeMutationFactory.createMutation(allele0, allele1, template, chance);
	}


	@Override
	public final IAllele[] getTemplate()
	{
        return Arrays.copyOf(template, template.length);
	}

	@Override
	@Nonnull
	public final IBeeGenome getGenome()
	{
		return genome;
	}

	@Override
	public final IBee getIndividual()
	{
		return new Bee(genome);
	}

	@Override
	public final ItemStack getMemberStack(@Nonnull EnumBeeType beeType)
	{
		IBee bee = getIndividual();
		return BeeManager.beeRoot.getMemberStack(bee, beeType);
	}

	public final IBeeDefinition getRainResist()
	{
		return new BeeVariation.RainResist(this);
	}

	public static void preInit() {
		MinecraftForge.EVENT_BUS.post(new AlleleSpeciesRegisterEvent<>(BeeManager.beeRoot, IAlleleBeeSpecies.class));
	}
}
