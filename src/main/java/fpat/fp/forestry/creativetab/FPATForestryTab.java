package fpat.fp.forestry.creativetab;

import fpat.fp.forestry.research.icons.FPATForestryResearchIcons;
import futurepack.client.creative.TabFB_Base;
import net.minecraft.item.ItemStack;

public class FPATForestryTab extends TabFB_Base {

	public FPATForestryTab(int id, String label) 
	{
		super(id, label);
	}

	
	@Override
	public ItemStack getTabIconItem()
	{
		return new ItemStack(FPATForestryResearchIcons.research_tab_pages,1,0);
	}
}
