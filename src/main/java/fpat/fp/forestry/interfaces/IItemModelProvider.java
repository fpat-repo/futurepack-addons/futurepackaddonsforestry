package fpat.fp.forestry.interfaces;

import net.minecraft.item.Item;

public interface IItemModelProvider {

	void registerItemModel(Item item);

}
