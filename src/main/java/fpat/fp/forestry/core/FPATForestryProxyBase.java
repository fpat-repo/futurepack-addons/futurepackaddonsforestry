package fpat.fp.forestry.core;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import fpat.fp.forestry.handler.BeeDefinition;
import fpat.fp.forestry.handler.FPATForestryRecipes;
import fpat.fp.forestry.handler.FPATForestryWorld;
import fpat.fp.forestry.items.comb.FPATEnumHoneyComb;
import fpat.fp.forestry.research.FPATForestryResearchLoader;
import futurepack.common.FPLog;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class FPATForestryProxyBase
{
	public void preInit(FMLPreInitializationEvent event) 
	{
		FPLog.initLog();
		
	}

	public void registerItemRenderer(Item item, int meta, String id) 
	{
		
	}
	
	public void load(FMLInitializationEvent event)
	{	    
		if(Loader.isModLoaded("forestry"))
		{
	    FPATForestryResearchLoader.init();
		BeeDefinition.initBees();
	    FPATForestryWorld.RegisterHives();
	    FPATForestryRecipes.Centrifuge();
		}

	}
	
	public void postInit(FMLPostInitializationEvent event)
	{
	}
}
