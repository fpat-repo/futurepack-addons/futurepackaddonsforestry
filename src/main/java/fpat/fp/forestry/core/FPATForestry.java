package fpat.fp.forestry.core;

import java.io.File;
import javax.annotation.Nullable;

import com.google.common.base.Preconditions;

//import fpat.fp.core.config.ConfigHandler;
//import fpat.fp.core.config.ConfigurationMain;
import fpat.fp.forestry.creativetab.FPATForestryTab;
import fpat.fp.forestry.handler.BeeDefinition;
import fpat.fp.forestry.handler.FPATForestryItemsCombs;
import fpat.fp.forestry.handler.FPATForestryRecipes;
import fpat.fp.forestry.handler.FPATForestryWorld;
import fpat.fp.forestry.research.FPATForestryResearchLoader;
import futurepack.client.creative.TabFB_Base;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.Mod.InstanceFactory;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;

@Mod(modid = FPATForestry.modID, name = FPATForestry.modName, version = FPATForestry.modVersion, dependencies = "required-after:fp;" + "after:forestry;" + "required-after:fpatcore")
public class FPATForestry
{
	public static final String modID = "fpatforestry";
	public static final String modName = "Futurepack Addon - Forestry";
	public static final String modVersion = "Version.version";

	@Instance(FPATForestry.modID)
	public static FPATForestry instance;

//	public static ConfigHandler configHandler;
	

	@Nullable
	private static FPATForestryItemsCombs items;
	
	public static FPATForestryItemsCombs getItems() {
//		Preconditions.checkState(items != null);
		return items;
	}


	public void registerItemsAndBlocks() {
		items = new FPATForestryItemsCombs();
//		blocks = new BlockRegistryApiculture();
	}
	
	public FPATForestry()
	{
		FluidRegistry.enableUniversalBucket();
	}
	

	public static TabFB_Base FPTab = new FPATForestryTab(CreativeTabs.getNextID(), "FPATForestryTab");


	@SidedProxy(modId=FPATForestry.modID, clientSide="fpat.fp.forestry.core.FPATForestryProxyClient", serverSide="fpat.fp.forestry.core.FPATForestryProxyServer")
	public static FPATForestryProxyBase proxy;

	@EventHandler
    public void preInit(FMLPreInitializationEvent event) 
    {	
//		File configFile = new File(event.getModConfigurationDirectory(), "futurepack-addon-team/forestry/main.conf");
//		configHandler = new ConfigHandler(configFile);
//		configHandler.addConfigurable(new ConfigurationMain());
    	proxy.preInit(event);
    }
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		if(Loader.isModLoaded("forestry"))
		{
			FPATForestryItemsCombs items = getItems();
			proxy.load(event);
		}
//		configHandler.reload(true);
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		proxy.postInit(event);
	}
	
	@EventHandler
	public void preServerStart(FMLServerAboutToStartEvent event) 
	{
		
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) 
	{		

	}
	
	@EventHandler
	public void serverStarted(FMLServerStartedEvent event) 
	{

	}
	
	
	@EventHandler
	public void serverStopped(FMLServerStoppingEvent event) 
	{

	}
}