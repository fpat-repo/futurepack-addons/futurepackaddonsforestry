package fpat.fp.forestry.items.frame;

import java.util.List;

import javax.annotation.Nullable;

import com.mojang.realmsclient.gui.ChatFormatting;

import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IBeeHousing;
import forestry.api.apiculture.IBeeModifier;
import forestry.api.apiculture.IHiveFrame;
import forestry.core.utils.Translator;
import fpat.fp.forestry.core.FPATForestry;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemsHiveFrame extends Item implements IHiveFrame, IBeeModifier
{
	private ItemsHiveFrame type;

	public float territoryMod = 1F;
	public float mutationMod = 1F;
	public float lifespanMod = 1F;
	public float productionMod = 1F;
	public float floweringMod = 1F;
	public float geneticDecay = 0.3F;
	public boolean isSealed = false;
	public boolean isSelfLighted = false;
	public boolean isSunlightSimulated = false;
	public boolean isHellish = false;
	public int maxDamage = 1000;

	public void FPFAItemHiveFrame(ItemsHiveFrame type)
	{
		this.type=type;
		this.setMaxStackSize(1);
		this.setMaxDamage(type.maxDamage);
		setCreativeTab(FPATForestry.FPTab);
	}
	
	@Override
	public float getTerritoryModifier(IBeeGenome genome, float currentModifier)
	{
		return type.territoryMod;
	}

	@Override
	public float getMutationModifier(IBeeGenome genome, IBeeGenome mate, float currentModifier) 
	{
		return type.mutationMod;
	}

	@Override
	public float getLifespanModifier(IBeeGenome genome, IBeeGenome mate, float currentModifier)
	{
		return type.lifespanMod;
	}

	@Override
	public float getProductionModifier(IBeeGenome genome, float currentModifier)
	{
		return type.productionMod;
	}

	@Override
	public float getFloweringModifier(IBeeGenome genome, float currentModifier)
	{
		return type.floweringMod;
	}

	@Override
	public float getGeneticDecay(IBeeGenome genome, float currentModifier)
	{
		return type.geneticDecay;
	}

	@Override
	public boolean isSealed()
	{
		return type.isSealed;
	}
	
	@Override
	public boolean isSelfLighted()
	{
		return type.isSelfLighted;
	}
	
	@Override
	public boolean isSunlightSimulated()
	{
		return type.isSunlightSimulated;
	}

	@Override
	public boolean isHellish()
	{
		return type.isHellish;
	}

	@Override
	public ItemStack frameUsed(IBeeHousing housing, ItemStack frame, IBee queen, int wear)
	{
		
		
		frame.setItemDamage(frame.getItemDamage() + wear);
		if(frame.getItemDamage() > frame.getMaxDamage())
		{
			frame = null;
		}
		return frame;
	}

	@Override
	public IBeeModifier getBeeModifier()
	{
		return this;
	}
	
//	@Override
//	public void addInformation(ItemStack stack, World w, List<String> tooltip, ITooltipFlag advanced)
//	{
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.production", getProductionModifier(null, 1F)));
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.genetic.decay", getGeneticDecay(null, 1F)));
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.flowering", getFloweringModifier(null, 1F)));
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.lifepsan", getLifespanModifier(null,null, 1F)));
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.mutation", getMutationModifier(null,null, 1F)));
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.territory", getTerritoryModifier(null, 1F)));
//		
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.sealed", isSealed()));
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.selflighted", isSelfLighted()));
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.sunlight.simulated", isSunlightSimulated()));
//		tooltip.add(Translator.translateToLocalFormatted("item.for.bee.modifier.hellish", isHellish()));
//		
//		super.addInformation(stack, w, tooltip, advanced);
//	}
	
	public ItemsHiveFrame setTerritoryMod(float territoryMod)
	{
		this.territoryMod = territoryMod;
		return this;
	}
	
	public ItemsHiveFrame setMutationMod(float mutationMod)
	{
		this.mutationMod = mutationMod;
		return this;
	}
	
	public ItemsHiveFrame setLifespanMod(float lifespanMod)
	{
		this.lifespanMod = lifespanMod;
		return this;
	}
	
	public ItemsHiveFrame setProductionMod(float productionMod)
	{
		this.productionMod = productionMod;
		return this;
	}
	
	public ItemsHiveFrame setFloweringMod(float floweringMod) 
	{
		this.floweringMod = floweringMod;
		return this;
	}
	
	public ItemsHiveFrame setGeneticDecay(float geneticDecay)
	{
		this.geneticDecay = geneticDecay;
		return this;
	}
	
	public ItemsHiveFrame setSealed(boolean isSealed)
	{
		this.isSealed = isSealed;
		return this;
	}
	
	public ItemsHiveFrame setSelfLighted(boolean isSelfLighted) 
	{
		this.isSelfLighted = isSelfLighted;
		return this;
	}
	
	public ItemsHiveFrame setSunlightSimulated(boolean isSunlightSimulated)
	{
		this.isSunlightSimulated = isSunlightSimulated;
		return this;
	}
	
	public ItemsHiveFrame setHellish(boolean isHellish)
	{
		this.isHellish = isHellish;
		return this;
	}
}
