package fpat.fp.forestry.items.comb;

import java.awt.Color;
import java.util.Locale;

import net.minecraft.util.IStringSerializable;

public enum FPATEnumHoneyComb implements IStringSerializable {

	NEON(new Color(0xe8d56a), new Color(0x45d0d0)),
	RETIUM(new Color(0x674016), new Color(0xff0404)),
	GLOWTITE(new Color(0x981919), new Color(0xf8aa0d)),
	BIOTERIUM(new Color(0xc8be67), new Color(0x00f400)),
	ALUTINE(new Color(0xf9ffff), new Color(0xe0e0e0)),
	MAGNETIC(new Color(0xdc7613), new Color(0x434343));
	//""(new Color(0xd7bee5), new Color(0xfd58ab)); // kindof pinkish

	public static final FPATEnumHoneyComb[] VALUES = values();

	public final String name;
	public final int primaryColor;
	public final int secondaryColor;
	private final boolean secret;

	FPATEnumHoneyComb(Color primary, Color secondary) {
		this(primary, secondary, false);
	}

	FPATEnumHoneyComb(Color primary, Color secondary, boolean secret) {
		this.name = toString().toLowerCase(Locale.ENGLISH);
		this.primaryColor = primary.getRGB();
		this.secondaryColor = secondary.getRGB();
		this.secret = secret;
	}
	
	@Override
	public String getName() {
		return name;
	}

	public boolean isSecret() {
		return secret;
	}
	
	public static FPATEnumHoneyComb get(int meta){
		if(meta >= VALUES.length){
			meta = 0;
		}
		return VALUES[meta];
	}
}
