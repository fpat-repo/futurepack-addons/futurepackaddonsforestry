package fpat.fp.forestry.items.comb;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import forestry.api.core.IModelManager;
import forestry.apiculture.items.ItemHoneyComb;
import forestry.core.config.Config;
import forestry.core.items.IColoredItem;
import forestry.core.items.ItemForestry;
import fpat.fp.forestry.core.FPATForestry;
import fpat.fp.forestry.handler.FPATForestryRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FPATItemsCombs extends ItemHoneyComb {
	public FPATItemsCombs() {
		setMaxDamage(0);
		setHasSubtypes(true);
		setCreativeTab(FPATForestry.FPTab);
	}
	@Override
	public String getUnlocalizedName(ItemStack stack) {
		FPATEnumHoneyComb honeyComb = FPATEnumHoneyComb.get(stack.getItemDamage());
		return "fpat" + "." + honeyComb.name;
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> subItems) {
		if (this.isInCreativeTab(tab)) {
			for (int i = 0; i < FPATEnumHoneyComb.VALUES.length; i++) {
				FPATEnumHoneyComb honeyComb = FPATEnumHoneyComb.get(i);
				if (!honeyComb.isSecret() || Config.isDebug) {
					subItems.add(new ItemStack(this, 1, i));
				}
			}
		}
	}
	
	@Nullable
	private static FPATEnumHoneyComb getRandomCombType(Random random, boolean includeSecret) {
		List<FPATEnumHoneyComb> validCombs = new ArrayList<>(FPATEnumHoneyComb.VALUES.length);
		for (int i = 0; i < FPATEnumHoneyComb.VALUES.length; i++) {
			FPATEnumHoneyComb honeyComb = FPATEnumHoneyComb.get(i);
			if (!honeyComb.isSecret() || includeSecret) {
				validCombs.add(honeyComb);
			}
		}

		if (validCombs.isEmpty()) {
			return null;
		} else {
			return validCombs.get(random.nextInt(validCombs.size()));
		}
	}

	public ItemStack getRandomComb(int amount, Random random, boolean includeSecret) {
		FPATEnumHoneyComb honeyComb = getRandomCombType(random, includeSecret);
		if (honeyComb == null) {
			return ItemStack.EMPTY;
		}
		return get(honeyComb, amount);
	}

	public ItemStack get(FPATEnumHoneyComb honeyComb, int amount) {
		return new ItemStack(this, amount, honeyComb.ordinal());
	}

	@Override
	public int getColorFromItemstack(ItemStack itemstack, int tintIndex) {
		FPATEnumHoneyComb honeyComb = FPATEnumHoneyComb.get(itemstack.getItemDamage());
		if (tintIndex == 1) {
			return honeyComb.primaryColor;
		}
		else {
			return honeyComb.secondaryColor;
		}
	}
}
