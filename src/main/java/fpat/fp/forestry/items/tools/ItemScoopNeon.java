package fpat.fp.forestry.items.tools;

import java.util.List;

import com.mojang.realmsclient.gui.ChatFormatting;

import forestry.api.core.IToolScoop;
import forestry.core.items.ItemForestryTool;
import forestry.core.items.ItemScoop;
import fpat.fp.forestry.core.FPATForestry;
import futurepack.api.interfaces.IItemNeon;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemScoopNeon extends ItemScoop implements IItemNeon
{
	public ItemScoopNeon(ToolMaterial NEON) {
		super();
		setCreativeTab(FPATForestry.FPTab);
		setMaxStackSize(1);
		setHarvestLevel("scoop", 0);
		setUnlocalizedName("scoop_neon");
	}
	
	@Override
	public float getDestroySpeed(ItemStack it, IBlockState b)
    {
        return b.getMaterial()==Material.IRON && getNeon(it)>0 ? 250F : 1.0F;
    }
	
	@Override
	public boolean onBlockDestroyed(ItemStack it, World w, IBlockState state, BlockPos pos, EntityLivingBase p_150894_7_)
	{
		if (state.getBlockHardness(w, pos) > 0.0D && getNeon(it)>0)
		{
			addNeon(it, -50);
		}	
		 
		return true;
	}

	@Override
	public int getMaxNeon(ItemStack it) 
	{
		return 800;
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRGB(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public boolean isNeonable(ItemStack it)
	{
		return true;
	}

	@Override
	public void addNeon(ItemStack it, int i)
	{
		int ne = getNeon(it) + i;
		if(ne<0)
			ne=0;
		
		NBTTagCompound nbt = it.getSubCompound("neon");
		if(nbt==null)
		{
			nbt = new NBTTagCompound();		
		}
		nbt.setInteger("ne", ne);
		it.setTagInfo("neon", nbt);
	}

	@Override
	public int getNeon(ItemStack it)
	{
		NBTTagCompound nbt = it.getSubCompound("neon");
		if(nbt==null)
		{
			return 0;
		}
		return nbt.getInteger("ne");
	}
	
	@Override
	public void addInformation(ItemStack it, World w, List l, ITooltipFlag p_77624_4_) 
	{
		l.add(getNeon(it) + "/" + getMaxNeon(it));
		super.addInformation(it, w, l, p_77624_4_);
	}
}
