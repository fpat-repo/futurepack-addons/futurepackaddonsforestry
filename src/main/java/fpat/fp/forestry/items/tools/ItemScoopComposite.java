package fpat.fp.forestry.items.tools;

import forestry.api.core.IToolScoop;
import forestry.core.items.ItemForestryTool;
import fpat.fp.forestry.core.FPATForestry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;

public class ItemScoopComposite extends ItemForestryTool implements IToolScoop
{
	public ItemScoopComposite(ToolMaterial COMPOSITE) {
		super(ItemStack.EMPTY);
		setCreativeTab(FPATForestry.FPTab);
		setMaxStackSize(1);
		setHarvestLevel("scoop", 0);
		setEfficiencyOnProperMaterial(5.0f);
		setUnlocalizedName("scoop_composite");
	}
}
