package fpat.fp.forestry.apiculture.hive;

import java.util.Locale;

import net.minecraft.util.IStringSerializable;

import javax.annotation.Nonnull;

import fpat.fp.forestry.core.FPATForestry;


public enum HiveType implements IStringSerializable {
	Menelaus(FPATForestry.modID + ":hivemenelaus",FPATForestry.modID + ".speciesmenelaus"),
	Tyros(FPATForestry.modID + ":hivetyros",FPATForestry.modID + ".speciesmenelaus"),
	Cybernetics(FPATForestry.modID + ":hivecybernetics",FPATForestry.modID + ".speciesmenelaus");

	public static final HiveType[] VALUES = values();

	@Nonnull
	private final String hiveUid;
	@Nonnull
	private final String speciesUid;

	HiveType(@Nonnull String hiveUid, @Nonnull String speciesUid) {
		this.hiveUid = hiveUid;
		this.speciesUid = speciesUid;
	}

	@Nonnull
	public String getHiveUid() {
		return hiveUid;
	}

	@Nonnull
	public String getSpeciesUid() {
		return speciesUid;
	}

	@Override
	public String getName() {
		return name().toLowerCase(Locale.ENGLISH);
	}

	public int getMeta() {
		return ordinal();
	}

}
