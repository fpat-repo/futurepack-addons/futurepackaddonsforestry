package fpat.fp.forestry.apiculture.hive;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.hives.HiveManager;
import forestry.api.apiculture.hives.IHiveDescription;
import forestry.api.apiculture.hives.IHiveGen;
import forestry.api.core.BiomeHelper;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.EnumTolerance;
import fpat.fp.forestry.handler.BeeDefinition;
import fpat.fp.forestry.handler.FPATForestryBlocksHives;
import futurepack.common.block.FPBlocks;

import javax.annotation.Nonnull;

public enum HiveDescription implements IHiveDescription 
{

	MENELAUS(HiveType.Menelaus, 2.0f, BeeDefinition.Menelaus, HiveManager.genHelper.ground(FPBlocks.sand)) {
		@Override
		public boolean isGoodBiome(Biome biome) {
			return BiomeDictionary.hasType(biome, BiomeDictionary.Type.getType("menelaus", Type.HOT, Type.DRY, Type.SANDY, Type.HILLS));
		}
	},
	
	TYROS(HiveType.Tyros, 2.0f, BeeDefinition.Tyros, HiveManager.genHelper.ground(FPBlocks.leaves)) {
		@Override
		public boolean isGoodBiome(Biome biome) {
			return BiomeDictionary.hasType(biome, BiomeDictionary.Type.getType("tyros", Type.HOT, Type.WET, Type.JUNGLE));
		}
	};
//	CYBERNETICS(IHiveRegistry.HiveType.END, 2.0f, BeeDefinition.ENDED, HiveManager.genHelper.ground(Blocks.END_STONE, Blocks.END_BRICKS)) {
//		@Override
//		public boolean isGoodBiome(Biome biome) {
//			return BiomeDictionary.hasType(biome, BiomeDictionary.Type.END);
//		}
//	};
	private static final IHiveGen groundGen = HiveManager.genHelper.ground(Blocks.SAND);
	private static final List<IBlockState> OreStates = new ArrayList<>();
	

	static 
	{
		OreStates.addAll(Blocks.CACTUS.getBlockState().getValidStates());	
	}

	private final IBlockState blockState;
	private final float genChance;
	private final IBeeGenome beeGenome;
	private final IHiveGen hiveGen;

	HiveDescription(HiveType hiveType, float genChance, BeeDefinition beeTemplate, IHiveGen hiveGen) 
	{
		this.blockState = FPATForestryBlocksHives.menelaus_hive.getStateForType(hiveType);
		this.genChance = genChance;
		this.beeGenome = beeTemplate.getGenome();
		this.hiveGen = hiveGen;
	}

	@Override
	@Nonnull
	public IHiveGen getHiveGen() 
	{
		return hiveGen;
	}

	@Override
	@Nonnull
	public IBlockState getBlockState() 
	{
		return blockState;
	}

	@Override
	public boolean isGoodBiome(@Nonnull Biome biome)
	{
		return !BiomeHelper.isBiomeHellish(biome);
	}

	@Override
	public boolean isGoodHumidity(@Nonnull EnumHumidity humidity)
	{
		
		EnumHumidity idealHumidity = beeGenome.getPrimary().getHumidity();
		EnumTolerance humidityTolerance = beeGenome.getToleranceHumid();
		return AlleleManager.climateHelper.isWithinLimits(humidity, idealHumidity, humidityTolerance);
	}

	@Override
	public boolean isGoodTemperature(@Nonnull EnumTemperature temperature)
	{
		EnumTemperature idealTemperature = beeGenome.getPrimary().getTemperature();
		EnumTolerance temperatureTolerance = beeGenome.getToleranceTemp();
		return AlleleManager.climateHelper.isWithinLimits(temperature, idealTemperature, temperatureTolerance);
	}

	@Override
	public float getGenChance() 
	{
		return genChance;
	}

	@Override
	public void postGen(@Nonnull World world, @Nonnull Random rand, @Nonnull BlockPos pos)
	{

	}

	protected static void postGenFlowers(World world, Random rand, BlockPos hivePos, List<IBlockState> flowerStates) 
	{
		int plantedCount = 0;
		for (int i = 0; i < 10; i++) {
			int xOffset = rand.nextInt(8) - 4;
			int zOffset = rand.nextInt(8) - 4;
			BlockPos blockPos = hivePos.add(xOffset, 0, zOffset);
			if ((xOffset == 0 && zOffset == 0) || !world.isBlockLoaded(blockPos)) {
				continue;
			}

			blockPos = groundGen.getPosForHive(world, blockPos.getX(), blockPos.getZ());
			if (blockPos == null) 
			{
				continue;
			}

			IBlockState state = flowerStates.get(rand.nextInt(flowerStates.size()));
			Block block = state.getBlock();
			if (!block.canPlaceBlockAt(world, blockPos)) 
			{
				continue;
			}
		
			world.setBlockState(blockPos, state);
			plantedCount++;
			if (plantedCount >= 5) {
				break;
			}
		}
	}
}
